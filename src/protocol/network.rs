use std::error::Error;
use async_trait::async_trait;

#[async_trait]
pub trait SerdeProtocol {
    /*
    take object and serialize to bytes
     */
    fn serialize(&self) -> Result<&[u8], Box<dyn Error>>;
    /*
    take bytes and deserialize to object
     */
    async fn deserialize(bytes:&[u8]) -> Self;    
}
