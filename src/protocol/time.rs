use chrono::{Duration, DateTime, Utc, NaiveDateTime, NaiveDate, NaiveTime};
use crate::protocol::tables::GPSTimeTable;
use std::error::Error;

/*
@python networktools

def gps_week2time(week, time_week):
# start date
try:
this_tz = pytz.timezone('UTC')
t0 = datetime(1980, 1, 6)
start_date = this_tz.localize(t0)
except Exception as e:
print("Error en generar start_date %s" % e)
raise e
# how many weeks and milliseconds after start_date
delta = timedelta(weeks=week, milliseconds=time_week)
final_date = start_date+delta
return final_date
 */
/// Calculated the final date using the week number started at January
/// 6 from 1980 and the week's milliseconds passed until the moment in
/// what the data is generated.
pub fn gps_week2time(
    week: i64,
    time: i64) -> Result<DateTime<Utc>,Box<dyn Error>>
{
    let date = NaiveDate::from_ymd_opt(1980, 1, 6).unwrap();
    let dtime = NaiveTime::from_hms_opt(0,0,0).unwrap();
    let start_date = DateTime::<Utc>::from_utc(NaiveDateTime::new(date,dtime), Utc);
    let final_data = start_date + Duration::weeks(week) + Duration::milliseconds(time);
    Ok(final_data)
}

/// Rescue the GPS Time data from TIME table and operate to calculate
/// the Datetime<Utc> value considering the leap seconds.
pub fn gpstime(
    table_time:&GPSTimeTable,
    leap: i64) -> Result<DateTime<Utc>,Box<dyn std::error::Error>>
{
    /*
    LEAP = leap
    time = data['TIME']
    GPS_WEEK = time['GPS_WEEK']
    GPS_TIME = time['GPS_TIME']  # miliseconds
    offset_time = timedelta(seconds=-LEAP)
    final_date = gps_week2time(GPS_WEEK, GPS_TIME)+offset_time
    #print("GPS TIME %s" %final_date)
     */
    let time = table_time.gps_time as i64;
    let week = table_time.gps_week as i64;
    let final_date = gps_week2time(week, time)? - Duration::seconds(leap);
    Ok(final_date)
}
