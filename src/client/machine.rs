use super::message;
use super::receiver;
use futures::future::{self};
use tokio::sync::broadcast::{self};
use tracing::{span, Instrument, Level};

#[doc = r#"
Create the instance for a socket client (TcpStream) connected with
the data receptor.

Uses data argument from cli to create the parameters.

Server Parameters:
- scheme: http/https
- host: localhost/ip
- port: valid port number

Receptor parameters:
- end-flag: &String

All the parameters must be simple string or numbers, so on the main
function (using clap mayb or args parsing) you can obtain or define
the values.
"#]
#[tracing::instrument(level = "info")]
pub async fn client(
    host: &String,
    port: &u16) {
    let (tx_source, _rx) = broadcast::channel(100);

    let addr =  format!("{}:{}", host, port);
    // couroutine to save the data
    let span_data = span!(Level::TRACE, "DATA");
    // couroutine to be used as client to receive data and
    // send to span_data
    let span_client = span!(Level::TRACE, "CLIENT");

    let recv_tx_source = tx_source.clone();

    let data_task = tokio::spawn(async move {
        message::receive_and_save_data(recv_tx_source)
            .instrument(span_data)
            .await;
    });

    let client_tx_source = tx_source.clone();

    let client_task = tokio::spawn(async move {
        receiver::run_client(&addr, client_tx_source)
            .instrument(span_client)
            .await;
    });

    let v = vec![data_task, client_task];
    let _outputs = future::try_join_all(v).await;
}
