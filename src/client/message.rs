use crate::protocol::gsof::GsofData;
use tokio::sync::broadcast::Sender;
use tracing::{error, info};


#[doc = r#"
This async function is connected with the function that manage the TcpStream
an receive the data as bytes by a broadcast channel.
Using a simple criteria with the end_flag agroup all slices received and
deserialize to create the structured d
ata. 
"#]
#[tracing::instrument(level = "info")]
pub async fn receive_and_save_data(
    tx_source: Sender<GsofData>) {
    info!("Starting receive and save data for");
    let mut rx_source = tx_source.subscribe();

    loop {
        match rx_source.recv().await {
            Ok(result) => {
                println!("{}", result.json_dumps());
                let latency = (result.latency() as f64) / 1000.0;
                println!("Latency {} secs", latency);                
            }
            Err(error) => {
                error!("Error on receive data, {}", error);
            }
        }
    }
}
