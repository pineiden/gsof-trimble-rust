extern crate clap;
extern crate gsof_protocol;

use clap::Parser;
use gsof_protocol::client::machine;
use tracing::info;
use tracing::{span, Instrument, Level};
use tracing_subscriber;


#[derive(Parser,Debug)]
#[clap(author, version, about, long_about=None)]
struct ConnectionArgs {
	// Give a hostname, url or ip
	#[clap(short, long, value_parser)]
	host:String,
	// Give a valid port number
	#[clap(short, long, value_parser, default_value_t = 2021)]
	port:u16
}

#[doc = r#"
This command allows you to connect to a TCP Socket server (ip and port provided)
Can receive any set of data separated by a sign like /*end*/.
So you have to detect or define what is the separator to manage correctly the
stream of data.
With the command /--help/ you can access to the options and default values.
```
run_socket --help
```
"#]
#[tokio::main]
pub async fn main() {
    tracing_subscriber::fmt::init();
    let span_main = span!(Level::TRACE, "main");
    info!("Starting client for datagen server");
    // obtener parametros de la linea de comandos
	let matches = ConnectionArgs::parse();

	info!("Connecting to {:?}", matches);

    machine::client(&matches.host, &matches.port)
        .instrument(span_main)
        .await;
}
