
#[doc = r#"
Check if a string can be a valid number for 16bits.
Than means from 0 to 65635. So can be use to validate port numbers and other
equivalents set.
Also the algorithm can be modified for more greates  numeric types.
"#]
pub fn isnumeric(val: String) -> Result<(), String> {
    match val.parse::<u16>() {
        Ok(n) => {
            if 1 <= n &&  n <= u16::MAX {
                return Ok(());
            };
            Err("Value must be from 1 to 65635".to_string())
        }
        Err(_e) => Err("Must be numeric value".to_string()),
    }
}
