use gsof_protocol::protocol::tables::read_gsof_file_protocol;
use gsof_protocol::protocol::gsof::{GsofData};
use tokio::{io};
use tracing::{span, Level, Instrument};
use tracing_subscriber;
use tracing::info;


/*
Read a stream of bytes from stdout and convert to GsofData

run_socket --help
 */
#[tokio::main]
async fn main(){
    tracing_subscriber::fmt::init();
	let span_main = span!(Level::TRACE, "main");
    let stdin = io::stdin();
    let mut reader = io::BufReader::new(stdin);
    let tables = read_gsof_file_protocol().unwrap();
	info!("Tables GSOF are {:?}", tables);
    loop {
		let span_main = span_main.clone();
        let result = GsofData::read_from_stream(
            &mut reader, &tables)
			.instrument(span_main)
			.await		
			.unwrap();
        println!("{}", result.json_dumps());
        let latency = (result.latency() as f64) / 1000.0;
        println!("Latency {} secs", latency);
    }
}
