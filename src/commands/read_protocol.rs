use gsof_protocol::protocol::tables::read_gsof_file_protocol;
use tracing::info;
use tracing::{span, Level};

#[tokio::main]
pub async fn main() {
    tracing_subscriber::fmt::init();
    let _span_main = span!(Level::TRACE, "main");
    let jsondata = read_gsof_file_protocol().unwrap();
    info!("{:#?}", jsondata);
}
