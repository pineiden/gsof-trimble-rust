Gsof Protocol
==============

This *crate* understand the *GSOF Protocol* from Trimble, is used to communicate
data from GNSS Sources (GPS).

With the time I can implement all the tables, but now are only the useful for
the job purpose.

Can be used as command as stream 

 ```
nc HOST port | ./target/debug/read_stream
 
 ```

or connecting directly to the source.

```
./target/debug/read_socket --host HOST --port  PORT

```
 
 Also can be used as library with other crates. Only need a *BufReader* and
 import the **GsofData* struct.
 
 You can check the code to understand how to use.
 
 
